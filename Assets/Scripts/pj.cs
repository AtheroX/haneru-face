﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class pj : MonoBehaviour {

	//raycast
	private float floatHeight = 0;
	private float liftForce = 0;
	private float damping = 0;

	public Rigidbody2D rb;
	public float Vel;
	public static int distancia = 0;
	public static int altitud = 0;
	public Text dist_txt;
	public Text alt_txt;
	public Text dist_txt2;
	public Text alt_txt2;
	public bool pagar = false;
	public ParticleSystem Pedo_ps;
	public GameObject Pedo_go;
	public Text pedos_txt;
	public bool pedo_bool=false;
	public static int pedo_Cantidad;
	public float inverso_caida;
	public static bool lanzado=false;
	public bool contacto_suelo= false;
	public static bool perdiste = false;
	public Button btn_pause;
	public static float pedo_plus_x = 5f;
	public static float pedo_plus_y = 15f;
	public static float viento = 1f;
	public static int VecesTerminado = 0;
	public static int HighScore;
	public static int ActualScore;
	public static string Score;

	void Start () {
		rb = GetComponent<Rigidbody2D>();
		Time.timeScale = 1;
		lanzado = false;
		pedo_Cantidad = PlayerPrefs.GetInt ("Cantidad de pedos");
		pedo_plus_x = PlayerPrefs.GetFloat("Pedo_Plus_x");
		pedo_plus_y = PlayerPrefs.GetFloat("Pedo_Plus_y");
		viento = PlayerPrefs.GetFloat ("viento");
		VecesTerminado = PlayerPrefs.GetInt ("Terminados");
	}

	void Update () {

		PlayerPrefs.SetInt ("Terminados",VecesTerminado);

		RaycastHit2D hit = Physics2D.Raycast (transform.position, -Vector2.up);
		if (hit.collider != null) {
			float distance = Mathf.Abs (hit.point.y - transform.position.y);
			float heightError = floatHeight - distance;
			float force = liftForce * heightError - rb.velocity.y * damping;
			rb.AddForce (Vector3.up * force);
		
		}

		if (Input.GetKey (KeyCode.M)) {
			Cosas_J.dinero += 100000;
		}
		if (Input.GetKey (KeyCode.T)) {

			VecesTerminado+=10;
			Debug.Log ("caca:" + VecesTerminado);
		}


		Vel =  Mathf.Sqrt( Mathf.Pow(rb.velocity.x,2)+Mathf.Pow(rb.velocity.y,2));
		distancia = (int)this.transform.position.x;
		altitud = (int)this.transform.position.y;
		pedos_txt.text = "        : " + pedo_Cantidad;
		if (distancia > -1) {
			if (Cosas_M.Idioma == 0) {
				dist_txt.text = "Distancia : " + distancia;
				dist_txt2.text = "Distancia : " + distancia;
			}
			if (Cosas_M.Idioma == 1) {
				dist_txt.text = "Distance : " + distancia;
				dist_txt2.text = "Distance : " + distancia;
			}
		}
		if (altitud > -1) {
			if (Cosas_M.Idioma == 0) {
				alt_txt.text = "Altura : " + altitud;
				alt_txt2.text = "Altura : " + altitud;
			}
			if (Cosas_M.Idioma == 1) {
				alt_txt.text = "Height : " + altitud;
				alt_txt2.text = "Height : " + altitud;
			}
		}
			
		if (!lanzado) {
			perdiste = false;
			if (pedo_Cantidad == 0) {
				pedo_Cantidad = 1;
			}
		}

		if ((perdiste)&&(!pagar)) {
			pagado ();
		}

		if (Input.GetKeyDown (KeyCode.R)) {
			SceneManager.LoadScene ("Juego");
		}

		if ((Vel < 2)&&(lanzado)&&(contacto_suelo)) {
			rb.isKinematic = true;
			perdiste = true;
		}
		if (perdiste) {
			Button btn_2 = btn_pause.GetComponent<Button> ();
			btn_2.onClick.Invoke();
		}

		if (lanzado) {
			Pedo_go.SetActive (true);
		}

		if ((!contacto_suelo)&&(lanzado)) {

			rb.velocity += new Vector2(viento*Time.deltaTime,0);
		}


	}
	void FixedUpdate()
	{
		if ((Input.GetMouseButtonDown (0))||Input.GetKeyDown(KeyCode.Space)) {
			if (!rb.isKinematic) {
				pedo_bool = true;
			}
		}

		if ((!rb.isKinematic)&& (pedo_bool)) {
			if (pedo_Cantidad!= 0) {
				Pedo_ps.Play();
				pedo_Cantidad--;
				rb.velocity=new Vector2(rb.velocity.x*0.55f+pedo_plus_x,rb.velocity.y*0+pedo_plus_y);
			}
			pedo_bool = false;
		}

	}

	void OnMouseDown(){
		
		slingshot.tocat_static = true;

	}

	void OnMouseUp(){
		
		Invoke ("tiempo_espera", 1);
		lanzado = true;
	}
	void pagado()
	{
		Cosas_J.dinero += distancia;
		pagar= true;
		VecesTerminado++;
		ActualScore = distancia;
		if (ActualScore >= HighScore) {

			HighScore = ActualScore;
		
		}
		Score ="Distance: " + HighScore;
	}
	void OnTriggerEnter2D(Collider2D other){


		if (!contacto_suelo) {
			if (other.gameObject.tag == "Suelo") {
				contacto_suelo = true;
				if (viento >= 5) {
					rb.velocity = new Vector2 (rb.velocity.x * 0.5f, rb.velocity.y);
				}
			} else {
				contacto_suelo = false;			
			}
		}
		if (lanzado) {
			if (other.gameObject.tag == "Enemy_f") {
				rb.velocity = new Vector2 (rb.velocity.x * 0.25f + 15, rb.velocity.y * 0 + 15);
			}
			if (other.gameObject.tag == "Enemy_o") {
				rb.velocity = new Vector2 (rb.velocity.x * 0.25f + 20, rb.velocity.y * 0 + 20);
			}
			if (other.gameObject.tag == "Enemy_f_v") {
				rb.velocity = new Vector2 (rb.velocity.x * 0.25f + 25, rb.velocity.y * 0 + 25);
			}
			if (other.gameObject.tag == "Enemy_n") {
				rb.velocity = new Vector2 (rb.velocity.x * 0.25f + 9f, rb.velocity.y * 0 + 9f);
			}
			if (other.gameObject.tag == "Enemy_d") {
				rb.velocity = new Vector2 (rb.velocity.x * 0.15f + 50f, rb.velocity.y * 0 + 80f);
			}
		}
		/*if (other) {
		
			Destroy (other, 3);
		
		}*/
	}
		


	void OnTriggerExit2D (Collider2D other){
		if (contacto_suelo) {
			if (other.gameObject.tag == "Suelo") {
				contacto_suelo = false;
			} 
		}

	}
				

	void tiempo_espera()
	{
		lanzado = true;


	}
}