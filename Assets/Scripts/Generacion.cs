﻿using UnityEngine;
using System.Collections;

public class Generacion : MonoBehaviour {

	public GameObject pj_go;
	public GameObject bg;
	public GameObject bg_inicio;
	public Vector3 ins_bg;
	public GameObject suelo;
	public GameObject suelo_inicio;
	public Vector3 ins_suelo;
	public GameObject forz_suelo;
	public GameObject forz_suelo_inicio;
	public Vector3 ins_forz_suelo;
	public int Dist_x_forz;
	public GameObject forz_v;
	public GameObject forz_v_inicio;
	public Vector3 ins_forz_v;
	public int Dist_x_forz_v;
	public Vector3 ins_Otaku_suelo;
	public GameObject Otaku_suelo;
	public GameObject Otaku_suelo_inicio;
	public int Dist_x_Otaku;
	public Vector3 ins_Nerd_suelo;
	public GameObject Nerd_suelo;
	public GameObject Nerd_suelo_inicio;
	public int Dist_x_Nerd;
	public Vector3 ins_Drag_suelo;
	public GameObject Drag_suelo;
	public GameObject Drag_suelo_inicio;
	public int Dist_x_Drag;
	public GameObject target;
	public GameObject Nerds_Grupo;
	public GameObject Otakus_Grupo;
	public GameObject Forz_V_Grupo;
	public GameObject Forz_Grupo;
	public GameObject Drags_Grupo;
	public GameObject BG_Grupo;
	public GameObject Suelo_Grupo;
	public GameObject ObjetoDeFondo;
	public Vector3 ins_ObjetosDeFondo;
	public GameObject ObjetosDeFondoGrupo;
	public Fondo fondo_sc;

	// Use this for initialization
	void Start () {
		
		Dist_x_forz = Random.Range (30, 300);
		Dist_x_forz_v = Random.Range (100, 300);
		Dist_x_Otaku = Random.Range (150, 300);
		Dist_x_Nerd = Random.Range (50, 200);
		Dist_x_Drag = Random.Range (300, 600);

			crear ();
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	void crear(){
		for (int i = 0; i < 2444; i++) {
			ins_suelo = new Vector3 (suelo_inicio.transform.position.x + 9.77f * i, suelo_inicio.transform.position.y, suelo_inicio.transform.position.z);
			target=Instantiate (suelo, ins_suelo, Quaternion.identity) as GameObject;
			target.transform.parent = Suelo_Grupo.transform;

		}
		for (int i = 0; i < 400; i++) {
			ins_bg = new Vector3 (bg_inicio.transform.position.x + 60 * i, bg_inicio.transform.position.y, bg_inicio.transform.position.z);
			target=Instantiate (bg, ins_bg, Quaternion.identity) as GameObject;
			target.transform.parent = BG_Grupo.transform;

		}
		for (int i = 0; i < 200; i++) {

			ins_forz_suelo = new Vector3 (forz_suelo_inicio.transform.position.x + Dist_x_forz * i, forz_suelo_inicio.transform.position.y, forz_suelo_inicio.transform.position.z);
			target=Instantiate (forz_suelo, ins_forz_suelo, Quaternion.identity) as GameObject;
			target.transform.parent = Forz_Grupo.transform;

			ins_forz_v = new Vector3 (forz_v_inicio.transform.position.x + Dist_x_forz * i, forz_v_inicio.transform.position.y, forz_v_inicio.transform.position.z);
			target=Instantiate (forz_v, ins_forz_v, Quaternion.identity) as GameObject;
			target.transform.parent = Forz_V_Grupo.transform;

		}
		for (int i = 0; i < 200; i++) {

			ins_Otaku_suelo = new Vector3 (Otaku_suelo_inicio.transform.position.x + Dist_x_Otaku * i, Otaku_suelo_inicio.transform.position.y, Otaku_suelo_inicio.transform.position.z);
			target=Instantiate (Otaku_suelo, ins_Otaku_suelo, Quaternion.identity) as GameObject;
			target.transform.parent = Otakus_Grupo.transform;

		}
		for (int i = 0; i < 200; i++) {

			ins_Nerd_suelo = new Vector3 (Nerd_suelo_inicio.transform.position.x + Dist_x_Nerd * i, Nerd_suelo_inicio.transform.position.y, Nerd_suelo_inicio.transform.position.z);
			target=Instantiate (Nerd_suelo, ins_Nerd_suelo, Quaternion.identity) as GameObject ;
			target.transform.parent = Nerds_Grupo.transform;

		}
		for (int i = 0; i < 100; i++) {

			ins_Drag_suelo = new Vector3 (Drag_suelo_inicio.transform.position.x + Dist_x_Drag * i, Drag_suelo_inicio.transform.position.y, Drag_suelo_inicio.transform.position.z);
			target = Instantiate (Drag_suelo, ins_Drag_suelo, Quaternion.identity) as GameObject;
			target.transform.parent = Drags_Grupo.transform;
		}

		for (int i = 0; i < 500; i++) {
			target = Instantiate (ObjetoDeFondo, new Vector3(0*i,0*i,0), Quaternion.identity) as GameObject;
			fondo_sc = target.GetComponent<Fondo> ();
			fondo_sc.Cosa=Random.Range (1, 4);
			fondo_sc.IDes = 0 + i;
			target.transform.parent = ObjetosDeFondoGrupo.transform;

		}
	}
}