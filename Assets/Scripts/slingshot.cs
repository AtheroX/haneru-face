﻿using UnityEngine;
using System.Collections;

public class slingshot : MonoBehaviour {

	public bool Tirable = true;
	public Vector2 click;
	public Vector2 Soltado;
	public float multiplicador;
	public float minimo_estirado;
	public float maximo_estirado;
	public float potencia;
	public int potencia_lanzador ;
	public GameObject pj;
	public Rigidbody2D rb;
	public GameObject cam;
	public GameObject Potencia_go;
	public float potencia_lanzada_slider;
	public bool tocat=false;
	public static bool tocat_static=false;
	public float direccionx;
	public float direcciony;

	// Use this for initialization
	void Start () {
		potencia = 0;
		rb = pj.GetComponent<Rigidbody2D>();
		rb.isKinematic = true;
		Potencia_go.SetActive (false);
		potencia_lanzador = Tienda.Potencia_Lanzador;
		potencia_lanzador =  PlayerPrefs.GetInt ("Potencia_Lanzador");

	}
	
	// Update is called once per frame
	void Update () {
		PlayerPrefs.SetInt ("Potencia_Lanzador", potencia_lanzador);
		if (potencia_lanzador == 0) {
			potencia_lanzador = -100000;
		}
		tocat = tocat_static;

		potencia = Mathf.Sqrt(pj.transform.position.x*potencia_lanzador+pj.transform.position.y*potencia_lanzador) ;

		if (Tirable) {
			if ((Input.GetMouseButtonDown (0))&&(tocat)) {
				
				click = Input.mousePosition;
				Potencia_go.SetActive (true);
		
			}

			if (Input.GetMouseButtonUp (0)&&(tocat)) {
				Tirable =false;
				rb.isKinematic = false;
				Soltado = Input.mousePosition;
				var direccion = click - Soltado;
				direccion.Normalize ();
				potencia_lanzada_slider = potencia*Cosas_J.Potencia_Val*1.5f;
				direccionx = direccion.x * potencia_lanzada_slider;
				direcciony = direccion.y * potencia_lanzada_slider;
				rb.AddForce (new Vector2 (direccionx,direcciony));
				cam.GetComponent<camara> ().enabled = true;
				pj.GetComponent<FixedJoint2D> ().enabled = false;
				Potencia_go.SetActive (false);
			}
		}
	
}
}