﻿using UnityEngine;
using System.Collections;

public class drag : MonoBehaviour {
	public Vector2 dist;
	public Vector2 pos;
	public float posX;
	public float posY;
	public Vector2 curPos;
	public Vector2 worldPos;
	public float x;
	public float y;
	public GameObject Lanzador;
	public float timer = 0.1f;
	public bool activado=true;
	public GameObject Cuerdas;
	public AudioClip Soltar;
	public AudioSource source;

	void Awake(){
	
		source = GetComponent<AudioSource> ();
	}

	void Update(){
		dist = this.transform.position;
		if (timer == 0) {
			Debug.Log ("ring");
			Lanzador.GetComponent<slingshot> ().enabled = false;
		}
	}

	void OnMouseDown(){
			dist = Camera.main.WorldToScreenPoint (transform.position);
			posX = Input.mousePosition.x - dist.x;
			posY = Input.mousePosition.y - dist.y;
			timer = timer * -Time.deltaTime;
	}
	void OnMouseUp(){

		activado = false;
		Cuerdas.SetActive (false);
		if (!pj.lanzado) {
			source.PlayOneShot (Soltar, 1f);
		}
	}


	void OnMouseDrag(){
		if (activado) {
			x = Input.mousePosition.x - posX;
			y = Input.mousePosition.y - posY;

			if (x > Screen.width*0.3) {

				x = Screen.width*0.3f ;
			}
			if (x < Screen.width*0.05) {

				x = Screen.width*0.05f;
			}
			if (y > Screen.height*0.4) {

				y = Screen.height*0.4f;
			}
			if (y < Screen.height*0.17) {

				y = Screen.height*0.17f;
			}
			curPos = new Vector2 (x, y);  
			worldPos = Camera.main.ScreenToWorldPoint (curPos);
			transform.position = worldPos;
			this.GetComponent<drag> ().enabled = false;

		}
	}
}
