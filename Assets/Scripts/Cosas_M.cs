﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Cosas_M : MonoBehaviour {

	public static int Idioma = 1;
	public GameObject Idioma_Español;
	public GameObject Idioma_Ingles;
	public GameObject Menu_Pr;
	public GameObject Menu_idiom_atrasE;
	public GameObject Menu_idiom_atrasI;
	private RawImage Menu_Pr_img;
	public Texture Menu_Pr_E;
	public Texture Menu_Pr_I;
	public GameObject Menu_Tienda;
	private RawImage Menu_Tienda_img;
	public Texture Menu_Tienda_E;
	public Texture Menu_Tienda_I;
	public GameObject Menu_Opciones;
	private RawImage Menu_Opciones_img;
	public Texture Menu_Opciones_E;
	public Texture Menu_Opciones_I;
	public GameObject Menu_Tienda_Adel_E;
	public GameObject Menu_Tienda_Adel_I;
	public static bool Secreto_Trofeo_titulo = false;
	public bool DebugActivable = true;
	public bool DebugActivo = false;
	public int CantidadDeClicks;
	public float Timer = 2f;
	public GameObject DebugImagen;
	public GameObject AtrasEnDebugE;
	public GameObject AtrasEnDebugI;
	public MonoBehaviour GJ;
	public GameObject LeaderBoards;
	public GameObject Trophies;


	// Use this for initialization
	void Start () {
		Cosas_J.dinero =  PlayerPrefs.GetInt ("Dinero");
		Tienda.Potencia_Lanzador =  PlayerPrefs.GetInt ("Potencia_Lanzador");

	}
	// Update is called once per frame
	void Update () {
		//RESET
		if ((Input.GetKey (KeyCode.A)) && (Input.GetKey (KeyCode.S)) && (Input.GetKey (KeyCode.D)) && (Input.GetKey (KeyCode.F))) {

			PlayerPrefs.DeleteAll();
			Debug.Log ("RESET");
			pj.pedo_Cantidad = 1;
			Tienda.Potencia_Lanzador = -100000;
			Cosas_J.Potencia_Tiempo = 1.2f;
			pj.pedo_plus_x = 5f;
			pj.pedo_plus_y = 15f;
			pj.viento = 1;
			Cosas_J.dinero = 0;
			pj.VecesTerminado = 0;
			Tienda.Cant_Comprada = 0;
			Tienda.ResetTienda = true;
			Tienda.Comprables [0] = false;
			Tienda.Comprables [1] = false;
			Tienda.Comprables [2] = false;
			Tienda.Comprables [3] = false;
			Tienda.Comprables [4] = false;
			Tienda.Comprables [5] = false;
			Scroll_Bar.LentesCompradas = 0;
		}

		Menu_Pr_img = (RawImage)Menu_Pr.GetComponent<RawImage>();
		Menu_Tienda_img = (RawImage)Menu_Tienda.GetComponent<RawImage>();
		Menu_Opciones_img = (RawImage)Menu_Opciones.GetComponent<RawImage>();
		GJ = this.gameObject.GetComponent<GameJoltATope>();

		if (Input.GetKeyDown (KeyCode.S)) {
			Tienda.Tienda_Menu = 1;
		}

		if (Idioma == 0) {
			Idioma_Español.SetActive (true);
			Idioma_Ingles.SetActive (false);
			Menu_Pr_img.texture = (Texture)Menu_Pr_E;
			Menu_Tienda_img.texture = (Texture)Menu_Tienda_E;
			Menu_Opciones_img.texture = (Texture)Menu_Opciones_E;
			Menu_Tienda_Adel_E.SetActive (true);
			Menu_Tienda_Adel_I.SetActive (false);
			Menu_idiom_atrasE.SetActive (true);
			Menu_idiom_atrasI.SetActive (false);
			AtrasEnDebugE.SetActive (true);
			AtrasEnDebugI.SetActive (false);

		}
		if (Idioma == 1) {
			Idioma_Español.SetActive (false);
			Idioma_Ingles.SetActive (true);
			Menu_Pr_img.texture = (Texture)Menu_Pr_I;
			Menu_Tienda_img.texture = (Texture)Menu_Tienda_I;
			Menu_Opciones_img.texture = (Texture)Menu_Opciones_I;
			Menu_Tienda_Adel_E.SetActive (false);
			Menu_Tienda_Adel_I.SetActive (true);
			Menu_idiom_atrasE.SetActive (false);
			Menu_idiom_atrasI.SetActive (true);
			AtrasEnDebugE.SetActive (false);
			AtrasEnDebugI.SetActive (true);

		}
		if (CantidadDeClicks != 0) {
			Timer = Timer - Time.deltaTime;
		}
		if (!Haneru_API.IsSignedIn){
			GJ.enabled=false;
		}
		if (Timer <= 0) {
			CantidadDeClicks = 0;
			Timer = 2f;
		}
		if ((DebugActivable) && (CantidadDeClicks >= 10)) {
			DebugActivo = !DebugActivo;
			CantidadDeClicks = 0;
		}
		if (DebugActivo) {
			
			DebugImagen.SetActive (true);
		
		} else {

			DebugImagen.SetActive (false);

		}
		if (!Haneru_API.IsSignedIn) {
			LeaderBoards.SetActive (false);
			Trophies.SetActive (false);
		}




	}





	public void ingles(){
		Idioma = 1;
	}

	public void español(){
		Idioma = 0;
	}

	public void Juego(){
		SceneManager.LoadScene ("Juego");
		PlayerPrefs.SetInt ("Dinero", Cosas_J.dinero);

	}

	public void Entra_Pausa(){
		Time.timeScale = 0;
	}
	public void Sale_Pausa(){
		Time.timeScale = 1;
	}
	public void Menuses(){
		SceneManager.LoadScene ("Menuses");
		Tienda.Tienda_Menu = 0;
	}
	public void Menuses2(){
		SceneManager.LoadScene ("Menuses");
		Tienda.Tienda_Menu = 1;
	}
	public void Menuses3(){
		SceneManager.LoadScene ("Menuses");
		Tienda.Tienda_Menu = 2;
	}
	public void SincargaMenuses(){
		Tienda.Tienda_Menu = 0;
	}
	public void SincargaMenuses2(){
		Tienda.Tienda_Menu = 1;
	}
	public void SincargaMenuses3(){
		Tienda.Tienda_Menu = 2;
	}
	public void GamejoltLogin(){
		SceneManager.LoadScene ("Gamejolt");
	}
	public void Secreto_trofeo_titulo(){
		Secreto_Trofeo_titulo = true;
	}
	public void ClicksParaDebug(){
		CantidadDeClicks++;
	}
	public void Reset(){
		PlayerPrefs.DeleteAll();
		Debug.Log ("RESET");
		pj.pedo_Cantidad = 1;
		Tienda.Potencia_Lanzador = -100000;
		Cosas_J.Potencia_Tiempo = 1.2f;
		pj.pedo_plus_x = 5f;
		pj.pedo_plus_y = 15f;
		pj.viento = 1;
		Cosas_J.dinero = 0;
		pj.VecesTerminado = 0;
		Tienda.Cant_Comprada = 0;
		Tienda.ResetTienda = true;
		Tienda.Comprables [0] = false;
		Tienda.Comprables [1] = false;
		Tienda.Comprables [2] = false;
		Tienda.Comprables [3] = false;
		Tienda.Comprables [4] = false;
		Tienda.Comprables [5] = false;
		Scroll_Bar.LentesCompradas = 0;
	}
	public void ClicksAntiDebug(){
		CantidadDeClicks = 0;
	}
}