﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fondo : MonoBehaviour {

	//public bool Ovni =false;
	public bool Nube =false;
	public bool Planeta=false;
	public bool Meteoro=false;
	public int Random_Nube = 0;
	public int Random_Planeta = 0;
	public float altura_Nube;
	public float altura_Planeta;
	public Sprite Ovni_spr; 
	public Sprite[] Nubes;
	public Sprite[] Planetas; 
	public Sprite Meteoro_spr; 
	private SpriteRenderer SR;
	public float Alto;
	public float Ancho;
	public int Cosa;
	public static float[] distancia_x;
	public static float[] distancia_y;
	public int IDes;


	void Start () {
		//arreglar array
		distancia_x = new float[4];
		distancia_y = new float[4];
		//de int a bool
		/*if (Cosa == 0) {
			Ovni =true;
			Nube =false;
			Planeta=false;
			Meteoro=false;
		}*/
		if (Cosa == 1) {
			//Ovni =false;
			Nube =true;
			Planeta=false;
			Meteoro=false;
		}
		if (Cosa == 2) {
			//Ovni =false;
			Nube =false;
			Planeta=true;
			Meteoro=false;
		}
		if (Cosa == 3) {
			//Ovni =false;
			Nube =false;
			Planeta=false;
			Meteoro=true;
		}
		//de bool a int
		/*if (Ovni) {
			Cosa = 0;
		}*/
		if (Nube) {
			Cosa = 1;
		}
		if (Planeta) {
			Cosa = 2;
		}
		if (Meteoro) {
			Cosa = 3;
		}
		//distancia
		/*if (Cosa == 0) {
			distancia_x [0] = Random.Range (50f, 40f);
			distancia_y [0] = Random.Range (100f, 300f);
		}*/
		if (Cosa == 1) {
			distancia_x [1] = Random.Range (7.5f, 25f)*IDes;
			distancia_y [1] = Random.Range (5f, 40f);
			this.transform.Translate(new Vector3 (0+distancia_x [1],0+distancia_y [1],0));
		}
		if (Cosa == 2) {
			distancia_x [2] = Random.Range (100f, 420f)*IDes;
			distancia_y [2] = Random.Range (120f, 300f);
			this.transform.Translate(new Vector3 (0+distancia_x [2],0+distancia_y [2],0));
		}
		if (Cosa == 3) {
			distancia_x [3] = Random.Range (200f, 400f)*IDes;
			distancia_y [3] = Random.Range (100f, 300f);
			this.transform.Translate(new Vector3 (0+distancia_x [3],0+distancia_y [3],0));
		}

		//randoms
		SR = this.gameObject.GetComponent<SpriteRenderer>();
		if (Nube) {
			Random_Nube = Random.Range (-1, 3);
		}
		if (Planeta) {
			Random_Planeta = Random.Range (-1, 3);
		}
		//Resize

		if (Nube) {
			Alto = Random.Range (50, 600);
			Alto = Alto / 1000;
			Ancho = Alto / 1.35f;
		}
		if (Planeta) {
			Alto = Random.Range (400, 800);
			Alto = Alto / 1000;
			Ancho = Alto;
		}
		if (Meteoro) {
			Alto = Random.Range (100, 300);
			Alto = Alto / 1000;
			Ancho = Alto;
		}
		//limites
		if (Random_Nube <= -1) {
			Random_Nube = 0;	
		}
		if (Random_Nube >= 3) {
			Random_Nube = 2;	
		}
		if (Random_Planeta <= -1) {
			Random_Planeta = 0;	
		}
		if (Random_Planeta >= 3) {
			Random_Planeta = 2;	
		}
		//alturas
		altura_Nube = Random.Range (20, 70);
		altura_Planeta = Random.Range (80, 200);
	}

	
	// Update is called once per frame
	void Update () {
		if (Nube) {
			if (Random_Nube == 0) {
				SR.sprite = Nubes [0];
			}
			if (Random_Nube == 1) {
				SR.sprite = Nubes [1];
			}
			if (Random_Nube == 2) {
				SR.sprite = Nubes [2];
			}
		}
		if (Planeta) {
			if (Random_Planeta == 0) {
				SR.sprite = Planetas [0];
			}
			if (Random_Planeta == 1) {
				SR.sprite = Planetas [1];
			}
			if (Random_Planeta == 2) {
				SR.sprite = Planetas [2];
			}
		}
		if (Meteoro) {
			SR.sprite = Meteoro_spr;
		}
		this.gameObject.GetComponent<Transform> ().localScale = new Vector3 (Ancho, Alto, 0);
	}
}